(ns blogger2obraz.core
  "Convert Blogger XML to a bunch of plaintext files as used by Obraz,
Jekyll, and other static site generators."
  (:require [clojure.xml :as xml]
            [clojure.string :as string]
            clojure.java.io)
  (:gen-class :main true))


(def ^:dynamic *filename-date-format*
  (java.text.SimpleDateFormat. "YYYY-MM-dd"))


(def ^:dynamic *yaml-date-format*
  (java.text.SimpleDateFormat. "YYYY-MM-dd HH:mm:ss"))


(def ^:dynamic *obraz-permalink-date-format*
  (java.text.SimpleDateFormat. "YYYY/MM/dd"))


(def ^:dynamic *default-layout* "post")


(def ^:dynamic *usage*
  "Usage: lein run path/to/blogger.atom")


(defn entry? [el]
  "Returns true if an XML element is an <entry>."
  (= :entry (:tag el)))


(defn entry-categories [e]
  "Returns a sequence of categories (kinds) for an Atom entry.

  Common categories include :template, :settings, :post, :comment."
  (->> e
       :content
       (filter #(and (= :category (:tag %))
                     (= "http://schemas.google.com/g/2005#kind" (:scheme (:attrs %)))))
       (map (comp keyword
                  #(string/replace % #".*#" "")
                  :term
                  :attrs))))


(defn post? [e]
  "Returns true if an XML element is a post entry."
  (and (entry? e)
       (some #{:post} (entry-categories e))))


(defn children-with-tag
  "Returns a sequence of direct children elements each wrapped with
  the given tag and having optional attributes."
  ([tag-keyword parent]
     (->> (:content parent)
          (filter #(= tag-keyword (:tag %)))))
  ([tag-keyword attrs parent]
     (->> (:content parent)
          (filter (fn [child]
                    (and (= tag-keyword (:tag child))
                         (every? (fn [[aname avalue]]
                                   (= avalue (aname (:attrs child))))
                                 attrs)))))))


(defn first-child-with-tag
  "Returns the first child element (a map with :tag, :attrs, :content)
with the given tag and optional attributes."
  ([tag-keyword parent]
     (->> parent
          (children-with-tag tag-keyword)
          first))
  ([tag-keyword attrs parent]
     (->> parent
          (children-with-tag tag-keyword attrs)
          first)))


(defn maybe-join
  "Returns nil when the input is nil. Join strings otherwise."
  ([coll]
     (when coll
       (string/join coll)))
  ([sep coll]
     (when coll
       (string/join sep coll))))


(defn parse-datetime
  [s]
  (when (string? s)
    (javax.xml.bind.DatatypeConverter/parseDateTime s)))


(defn draft? [e]
  "Returns true, if an XML element is a draft (unpublished) entry."
  (and (entry? e)
       (->> e
            (first-child-with-tag :app:control)
            (first-child-with-tag :app:draft)
            :content
            (= ["yes"]))))


(defn post-title [e]
  "Returns a string with the title of the post."
  (->> e
       (first-child-with-tag :title {:type "text"})
       :content
       maybe-join))


(defn post-published [e]
  "Parses publication date and time of the post."
  (->> e
       (first-child-with-tag :published)
       :content
       maybe-join
       parse-datetime))


(defn post-updated [e]
  "Parses date and time of the last update of the post."
  (->> e
       (first-child-with-tag :updated)
       :content
       maybe-join
       parse-datetime))


(defn post-html [e]
  "Extracts HTML content of the post body."
  (->> e
       (first-child-with-tag :content {:type  "html"})
       :content
       (maybe-join "\n")))


(defn post-author [e]
  (->> e
       (first-child-with-tag :author)
       (first-child-with-tag :name)
       :content
       (maybe-join " ")))


(defn post-tags [e]
  "Returns a sequence of user tags (strings) for a post entry."
  (->> e
       :content
       (filter #(and (= :category (:tag %))
                     (= "http://www.blogger.com/atom/ns#" (:scheme (:attrs %)))))
       (map (comp :term :attrs))))


(defn post-permalink [e]
  "Returns a permanent location of the post."
  (->> e
       (first-child-with-tag :link {:rel "alternate" :type "text/html"})
       :attrs
       :href))


(defn parse-post [e]
  "Converts a post entry (an XML element) to a flat Clojure map
  suitable for generation of a plaintext file."
  (when (post? e)
    {:title (post-title e)
     :html (post-html e)
     :published? (not (draft? e))
     :published-date (post-published e)
     :updated-date (post-updated e)
     :permalink (post-permalink e)
     :tags (post-tags e)
     :author (post-author e)}))


(defn sha1
  [input-string]
  (let [md (java.security.MessageDigest/getInstance "SHA1")]
    (as-> input-string s
          (.getBytes s "UTF-8")
          (.digest md s)
          (map #(Integer/toHexString (bit-and 0xff %)) s)
          (apply str s))))


(defn plaintext-slug [parsed-post]
  "Extracts the slug from the original permalink."
  (let [plink (:permalink parsed-post)
        plink-slug (when plink
                     (string/replace plink #".*/([^/\.]+)\.html" "$1"))
        title (:title parsed-post)
        title-slug (delay
                    (when title
                      (-> (string/replace title #"[^\p{L}\p{Mn}\p{Digit}- ]" "")
                          (string/replace       #" " "-"))))
        sha1-slug (delay (sha1 (str parsed-post)))]
    (or plink-slug @title-slug @sha1-slug "")))


(defn plaintext-format-date [date-format pubdate]
  "Formats publication date according to date-format."
  (if pubdate
    (.format date-format
             (.getTime pubdate))
    ;; else use today's date
    (.format date-format
             (java.util.Date.))))


(defn plaintext-filename [parsed-post]
  "Returns filename of the file to write plaintext to."
  (let [pubdate (plaintext-format-date
                 *filename-date-format*
                 (:published-date parsed-post))
        slug    (plaintext-slug parsed-post)]
    (str "_posts/" pubdate "-" slug ".md")))


(defn plaintext-obraz-permalink [parsed-post]
  "Returns path of the new permalink."
  (let [pubdate (plaintext-format-date
                 *obraz-permalink-date-format*
                 (:published-date parsed-post))
        slug    (plaintext-slug parsed-post)]
    (str "/" pubdate "/" slug ".html")))


(defn format-tag [s]
  (-> s
      ;; replace unicode letter, non-spacing modifiers, digits, dashes and spaces
      (string/replace #"[^\p{L}\p{Mn}\p{Digit}- ]" "")
      ;; replace spaces with dashes (multiword tags)
      (string/replace " " "-")))


(defn escape-quotes [s]
  (if s
    (string/escape s {\" "\\\""})
    ""))


(defn plaintext-yaml-front-matter
  "Returns a YAML front-matter as a string."
  ([parsed-post]
     (plaintext-yaml-front-matter parsed-post *default-layout*))
  ([parsed-post layout]
     (let [p       parsed-post
           pubdate (plaintext-format-date
                    *yaml-date-format*
                    (:published-date p))
           update  (plaintext-format-date
                    *yaml-date-format*
                    (:updated-date p))
           tags    (let [ts (map format-tag (:tags p))]
                     (str "[ " (string/join ", " ts) " ]"))
           yaml    (string/join
                    "\n" ["---"
                          (format "title: \"%s\"" (escape-quotes (:title p)))
                          (format "date: %s" pubdate)
                          (format "tags: %s" tags)
                          (format "layout: %s" layout)
                          (format "published: %s" (:published? p))
                          (format "blogger-permalink: \"%s\"" (escape-quotes (:permalink p)))
                          (format "blogger-published: %s" pubdate)
                          (format "blogger-updated: %s" update)
                          (format "blogger-author: \"%s\"" (escape-quotes (:author p)))
                          ""
                          "---"])]
       yaml)))


(defn plaintext-post
  ([parsed-post]
     (plaintext-post parsed-post *default-layout*))
  ([parsed-post layout]
     (str (plaintext-yaml-front-matter parsed-post layout)
          "\n\n"
          (:html parsed-post))))


(defn convert-xml-to-plaintexts [xml-filename]
  (let [posts (->> (xml/parse xml-filename)
                   xml-seq
                   (filter post?)
                   (map parse-post))]
    (doseq [p posts]
      (let [text (plaintext-post p)
            text-filename (plaintext-filename p)
            oldplink (:permalink p)
            newplink (plaintext-obraz-permalink p)]
          (println (format "\"%s\" => \"%s\""
                           (escape-quotes oldplink)
                           (escape-quotes newplink)))
          (.mkdir (clojure.java.io/file "_posts"))
          (spit text-filename text)))))


(defn -main [& args]
  (cond
   (or (empty? args)
       (some #{"-h" "--help" "-help"} args))
   (do
     (println *usage*))
   :normally
   (doseq [fname args]
     (convert-xml-to-plaintexts fname))))
