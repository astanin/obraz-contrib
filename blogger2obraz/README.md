# blogger2obraz

A tool to convert an XML file (Atom) exported from Blogger to
plain-text text files with separate posts as used by Obraz, Jekyll,
and other static site generators.

## Usage

This program is written in Clojure, and installing
[Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
and [leiningen](http://leiningen.org/) is the easiest way to get it
running.

In the directory of this project run:

    lein run path/to/blogger.xml

where `path/to/blogger.xml` is path to the exported Atom XML of a Blogger blog.
You can download it from [Google Takeout](https://www.google.com/takeout/).

The program creates plain-text files compatible with Obraz and Jekyll
in `_posts` subdirectory, and prints mapping of the old Blogger
permalinks to new Obraz or Jekyll permalinks to the standard output.
You may use it to setup redirects.


## License

Copyright © 2013 Sergey Astanin

Distributed under the Eclipse Public License, the same as Clojure.
