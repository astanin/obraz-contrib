(defproject blogger2obraz "1.0-SNAPSHOT"
  :description "Convert Blogger export XML to Obraz/Jekyll plain-text posts."
  :url "http://bitbucket.org/astanin/obraz-contrib"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]]
  :main blogger2obraz.core)
