# -*- coding: utf-8 -*-

# Copyright (c) 2013 Sergey Astanin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Markdown extensions for Obraz.

To use this plugin:

  * put it into _plugins/ directory of your site

  * in site configuration (`_config.yml`) define a list of the markdown
    extensions to use. For instance,

        markdown:
          extensions:
            - fenced_code
            - tables
            - footnotes


Requirements:

  * Obraz >= 0.4

"""

import obraz
from markdown import markdown


@obraz.template_filter('markdownify')
@obraz.file_filter(['.md', '.markdown'])
def markdown_extra_filter(s, config):
    extensions = config.get("markdown",{}).get("extensions",[])
    return markdown(s, extensions)
