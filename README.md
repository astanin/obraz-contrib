# Obraz-contrib

User contributions to Obraz, a static blog generator.

   * [`obraz.el`][obraz.el]: an Emacs mode for using Obraz;
     moved to [a separate repository][obraz.el]

  * `blogger2obraz`: a migration script to convert a Blogger
    (Blogspot) blog to Obraz (or Jekyll)

  * `emacs/yasnippets/`: using yasnippets is a poor man's alternative to
     [obraz.el][obraz.el]

  * `plugins/`: a repository for Obraz plugins

      - `tags.py`: a plugin to generate separate pages for tags

      - `markdown_extra.py`: a plugin to configure which Markdown
        extensions are used


[obraz.el]: https://github.com/astanin/obraz.el
